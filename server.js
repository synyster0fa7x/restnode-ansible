const {
    exec
} = require('child_process');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 1337;
app.use(bodyParser.urlencoded({
    extended: false
}));
app.listen(port, '0.0.0.0');

app.post('/mumble', function (req, res) {
    if (req.body.username) {
        exec('ansible-playbook /etc/ansible/playbooks/playbook_mumble_creation.yml --extra_vars "other_variable=' + req.body.username + '"', (err, stdout, stderr) => {
            if (err) {
                res.send({
                    status: 'nok',
                    error: err
                });
                return;
            }

            res.send({
                status: 'ok',
                stdout: '${stdout}'
            })
        });
    } else {
        res.send({
            'status': 'nok',
            'message': 'username param is required'
        })
    }
});

app.delete('/mumble', function (req, res) {
    if (req.body.username) {
        exec('ansible-playbook /etc/ansible/playbooks/playbook_mumble_deletion.yml --extra_vars "other_variable=' + req.body.username + '"', (err, stdout, stderr) => {
            if (err) {
                res.send({
                    status: 'nok',
                    error: err
                });
                return;
            }

            res.send({
                status: 'ok',
                stdout: '${stdout}'
            })
        });
    } else {
        res.send({
            'status': 'nok',
            'message': 'username param is required'
        })
    }
});

console.log('Ansible API server started on: ' + port);